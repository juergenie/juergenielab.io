#!/bin/bash

echo 'run build...'
npx vuepress build docs --dest public
echo 'check status...'
git add .
git status
echo 'confirm to push? (y/n)'
read confirm
if [ $confirm == 'y' ]; then
  git commit -m 'published.'
  git push
  echo 'published.'
else
  echo 'cancel.'
fi

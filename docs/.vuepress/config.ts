import { defineUserConfig, DefaultThemeOptions } from "vuepress";

export default defineUserConfig<DefaultThemeOptions>({
  lang: "zh-CN",
  title: "Chrock Space",
  description: "JuerGenie的个人博客",

  theme: "@vuepress/theme-default",
  themeConfig: {},
});
